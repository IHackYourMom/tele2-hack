from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

import pickle
import pymorphy2
import re


from .models import Appeal
from .serializers import AppealSerializer


class DetailAppeal(generics.RetrieveUpdateDestroyAPIView):
    queryset = Appeal.objects.all()
    serializer_class = AppealSerializer

class getStatesTitles(APIView):

    def get(self, request):
        appeal = Appeal.objects.all()
        serializer = AppealSerializer(appeal, many=True)
        return Response(serializer.data)

    def post(self, request):
        appeal = Appeal.objects.filter(priority=request.data.get("priority"))
        newAppeal = AppealSerializer(appeal, many=True)

        return Response(newAppeal.data)

class getData(APIView):

    def normalize(self, text):
        morph = pymorphy2.MorphAnalyzer()

        if text.startswith("'"):
            text = text[1:]
        content = re.findall(r"[\w']+", text)

        # print("content", content)
        res = []
        for c in content:
            morphres = morph.parse(c)
            if len(morphres) > 0:
                res.append(morphres[0].normal_form)

        return ' '.join(res)


    def clustering(self, text):
        clf = None
        with open('/data/clusters.pkl', 'rb') as f:
            clf = pickle.load(f)
        vectorizer = None
        with open('/data/vectorizer.pkl', 'rb') as f:
            vectorizer = pickle.load(f)

        X = vectorizer.transform([self.normalize(text)])
        cls = clf.predict(X)

        return cls
    
    def prior(self, data):
        crit = [1, 5, 13, 19, 21, 13]
        high = [4, 7, 10, 11, 15, 17, 18, 23]

        for i in range(len(crit)):
            if data['topic'] == crit[i]:
                return 2

        for i in range(len(high)):
            if data['topic'] == high[i]:
                return 1

        return 0

    def post(self, request):
        data = request.data
        content = request.data.get("content")
        topic = self.clustering(content)
        data['topic'] = topic[0]
        data['priority'] = self.prior(data)

        newAppeal = AppealSerializer(data=data)

        if newAppeal.is_valid():
            newAppeal.save()
        else:
            return Response(newAppeal.errors)

        return Response(topic)
