from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Appeal(models.Model):

    vals=[MinValueValidator(0),
          MaxValueValidator(2)]

    content = models.CharField(max_length=200)
    priority = models.IntegerField(default=0,
                                validators = vals)

    datetime = models.DateTimeField(auto_now_add=True)
    user_id = models.IntegerField()
    topic = models.CharField(max_length=100)
    source_name = models.CharField(max_length=100)
    chat_id = models.IntegerField()
    location = models.CharField(max_length=100)
    author_category = models.CharField(max_length=100)
    subscribers = models.IntegerField()
    
    def __str__(self):
        return self.content
