from rest_framework import serializers
from .models import Appeal

class AppealSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
                'content',
                'priority',
                'datetime',
                'user_id',
                'topic',
                'source_name',
                'chat_id',
                'location',
                'author_category',
                'subscribers'
        )

        model = Appeal
