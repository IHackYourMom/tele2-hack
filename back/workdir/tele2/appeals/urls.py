from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
        path('', views.getStatesTitles.as_view()),
        path('<int:pk>/', views.DetailAppeal.as_view()),
        path('data/', views.getData.as_view()),
]
