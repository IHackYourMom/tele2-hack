import pickle
import pymorphy2
import re

morph = pymorphy2.MorphAnalyzer()

def normalize(text):
    if text.startswith("'"):
        text = text[1:]
    content = re.findall(r"[\w']+", text)

    # print("content", content)
    res = []
    for c in content:
        morphres = morph.parse(c)
        if len(morphres) > 0:
            res.append(morphres[0].normal_form)
    return ' '.join(res)

def main():
    clf = None
    with open('clusters.pkl', 'rb') as f:
        clf = pickle.load(f)
    vectorizer = None
    with open('vectorizer.pkl', 'rb') as f:
        vectorizer = pickle.load(f)


    req = "здравствовать каков срок действие номер с день активация или последний исходящий звонок как долго мочь не пользоваться прежде чем номер быть заблокировать"

    X = vectorizer.transform([normalize(req)])
    cls = clf.predict(X)
    print("cls", cls)



if __name__ == '__main__':
    main()
