import { Button } from 'reactstrap';
import React, { Component } from 'react';

const Bad = item => (
        <div class="alert alert-secondary" key={item.id}>
        <h3>{item.content}</h3>
        <Button class="btn">Ответить</Button>
        </div>
);

export default Bad;


