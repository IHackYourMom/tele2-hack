import { Button } from 'reactstrap';
import React, { Component } from 'react';

const Good = item => (
        <div class="alert alert-secondary" key={item.id}>
        	<h1>{item.content}</h1>
        	<Button class="btn">Ответить</Button>
        </div>
);

export default Good;

