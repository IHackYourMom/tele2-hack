import React, { Component } from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import Worst from './components/Worst';
import Bad from './components/Bad';
import Good from './components/Good';

class App extends Component {
	state = {
		good_appeals: [],
		bad_appeals: [],
		worst_appeals: [],
	};

	async componentDidMount() {
		try {

			/* Good Appleas */

			this.serverRequest = axios
			.post("http://192.168.11.107:21080/api/", {
					priority: 0
			}).then(res => {
				const good_appeals = res.data;
				this.setState({good_appeals});
			})

			/* Bad Appleas */

			this.serverRequest = axios
			.post("http://192.168.11.107:21080/api/", {
					priority: 1
			}).then(res => {
				const bad_appeals = res.data;
				this.setState({bad_appeals});
			})

			/* Worst Appleas */

			this.serverRequest = axios
			.post("http://192.168.11.107:21080/api/", {
					priority: 2
			}).then(res => {
					const worst_appeals = res.data;
					this.setState({worst_appeals});
			})
		
		} catch(e) {
			console.log(e);
		}
	}

	render() {
		return (
			<div class="row">
				<div class="col-xs-6 col-sm-4 d-inline-block alert alert-danger" role="alert">
					<h1 align="center">Ужасно</h1>
					{this.state.worst_appeals.map(item => (
						<div class="alert alert-secondary" key={item.id}>
                			<a>{item.content}</a>
							<br />
							<span>{item.datetime}</span>
							<br />
							<span>{item.chat_id}</span>
							<br />
							<span>{item.topic}</span>
							<br />
							<span>{item.source}</span>
                			<Button class="btn">Ответить</Button>
        				</div>

					))}

				</div>



				<div class="col-xs-6 col-sm-4 d-inline-block alert alert-warning" role="alert">
					<h1 align="center">Плохо</h1>
					{this.state.bad_appeals.map(item => (
						<div class="alert alert-secondary" key={item.id}>
        					<a>{item.content}</a>
                            <br />
                            <span>{item.datetime}</span>
                            <br />
                            <span>{item.chat_id}</span>
                            <br />
                            <span>{item.topic}</span>
                            <br />
                            <span>{item.source}</span>

        					<Button class="btn">Ответить</Button>
        				</div>

					))}

				</div>

				<div class="col-xs-6 col-sm-4 d-inline-block alert alert-success" role="alert">
					<h1 align="center">Норм</h1>
					{this.state.good_appeals.map(item => (
							<div class="alert alert-secondary" key={item.id}>
                				<a>{item.content}</a>
                            	<br />
                            	<span>{item.datetime}</span>
                            	<br />
                            	<span>{item.chat_id}</span>
                            	<br />
                            	<span>{item.topic}</span>
                            	<br />
                            	<span>{item.source}</span>

                				<Button class="btn">Ответить</Button>
        					</div>

					))}

				</div>
			</div>
		);
	}
}

export default App;

